#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 14:37:25 2020

@author: martynrittman
"""

import unittest
import mrced2

import sys
sys.path.insert(0, '..')

class testEventData(unittest.TestCase):
    
    def test_00_BuildQuery(self):
        '''
        
        Test building a query. The output should be a printed query that includes a well-formatted API call.
        
        '''
        
        mc = mrced2.eventData(rows = 50, mailto = "test@crossref.org", filename = "basic_test.json")
        
        mc.buildQuery({})

    def testRunquery(self):
        '''
        
        Test to query the API. Takes some time, so commented out.
        
        '''
        
        mc = mrced2.eventData(rows = 50, mailto = "test@crossref.org", filename = "basic_test.json")
        
        mc.buildQuery({})
        mc.runQuery(retry = 5)
        
    def test_10_LoadJson(self):
        '''
        
        Test loading a json file. The output is the number of total results in the query used to 
        generate the test data (538409097) and the first 5 events in the file.
        
        '''
        
        jd = mrced2.eventRecord(filename = "test.json")
        
        jd.getHits()
        jd.displayEvents(5)
        
    def test_20_Filtering(self):
        '''
        
        Test filtering some data from a test json file. The outputs should be the number of twitter events (10),
        a dictionary {'twitter': 10} and another dictionary with predefined bins, some of which are empty:
        {'twitter': 10, 'crossref': 0, 'datacite': 0}.
        
        '''
        
        # load test data
        jd= mrced2.eventRecord(filename = "test.json")
        
        # filter by source ID
        filtered_jd = jd.filterEvents({'source_id' : "twitter"})
        # show the number of results after filtering
        filtered_jd.getHits()
        
        # get counts for each source ID type
        hist1 = jd.eventHist("source_id")
        print(hist1)
        
        # get counts for each source ID type using predefined bins
        hist2 = jd.eventHist("source_id", bins = ["twitter", "crossref", "datacite"])
        print(hist2)
        
    def test_30_otherEndpoints(self):
        '''
        
        Test the Scholix and Deleted endpoints
        
        '''
        
        scholix_test = mrced2.eventData(mailto = 'test@crossref.org', filename = 'scholixtest.json')
        
        scholix_test.buildQuery({}, scholix = True)
                
        scholix_test.runQuery(scholix = True)
        scholix_test.events.getHits()
        scholix_test.events.getHits()
        
        deleted_test = mrced2.eventData(mailto = 'test@crossref.org', filename = 'deletedtest.json')
        deleted_test.buildQuery({}, deleted = True)
        deleted_test.runQuery()
        deleted_test.events.getHits()
        deleted_test.events.displayEvents(5)
        
     

if __name__ == '__main__':
    unittest.main()