# Event Data Notebooks

Here you will find some executable Jupyter notebooks to query different aspects of Crossref event data, along with Python code (mrced) which acts as a wrapper for the queries. Click the logos below to launch an interactive Jupyter notebook.

## Jupyter notebooks

1. Find any events related to a single DOI
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fsingle_doi_check.ipynb)

1. Check a website to see how many events it's generated
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fsource_website_check.ipynb)

1. Find the last 6 months' data classified by source [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fevent_data_statistics.ipynb)

1. Find events related to a single publisher
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fpublisher_events.ipynb)

2. Find events for a single author
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fauthor_events.ipynb)

1. Query the evidence log and evidence records to see how many domains were Tweeted about in a specific time, and how many observations became an event.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fevent_data_log_queries.ipynb)

1. Check the general status of Event Data, such as how many events have been added or updated recently and some key sources.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fstatus_check.ipynb)

1. Get software citations for a subject or object prefix.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/crossref%2Fevent_data_notebooks/master?filepath=examples%2Fsoftware_citations_search.ipynb)

## mrced - Getting Crossref event data from the API

The code includes a module for querying the Crossref event data API and some processing of the outputs.

There are several classes:

 - eventData: used to query the API
 - eventRecord: used to extract data from the resulting JSON files
 - activityLogs: query Event Data [activity logs](https://www.eventdata.crossref.org/guide/data/evidence-records/)
 - evidenceRecords: query Event Data [evidence records](https://www.eventdata.crossref.org/guide/data/evidence-logs/)
 - timeFix: some workarounds for handling months

 ### Simple example

```
 import mrced2

 ed = mrced2.eventData(mailto = "email@test.com")

 ed.buildQuery({'rows': 50}) # puts the query into the right format, returns the first 50 events
 ed.runQuery() # sends the query to the Crossref API server
 ed.events.displayEvents(10) # print a summary of the first 10 events to the screen
```

 buildQuery takes kwargs that correspond to options in the Crossref documentation. The output json filename can be changed through the mrced.eventData.outputFile variable (the default is test.json).

# Changelog

## Update on 2021-09-23

Breaking change! A number of functions have changed name and will no longer work.

### Changed

eventRecord.hist: changed to hist, updated mechanism for binning

The following have been deprecated
eventRecord.getHits: changed to count (old command still available)

The following have been renamed and will no longer work
eventRecord.histEvents: changed to hist
eventRecord.displayEvents: changed to print
eventRecord.searchEvents: changed to search
